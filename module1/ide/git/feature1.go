package main

import (
	"fmt"
	"strconv"
)

/*
	func sliceText(i string) {
		sliced := strings.Split(i, "")
		fmt.Println(sliced)
	}
*/

func rep(name string) {
	nameInt, _ := strconv.Atoi(name)
	fmt.Printf("%T", nameInt)

}
func feature1() {
	stringOne := "CHECK OUT MY FEATURE!"
	rep(stringOne)
}
