package algo

import (
	"reflect"
	"testing"
)

func BenchmarkBubbleSort(b *testing.B) {
	data := []int{55, 44, 32, 21, 14, 5, 4, 4, 4, 4, 3, 2, 1}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(data)
	}
}

func BenchmarkBubbleSortOptimized(b *testing.B) {
	data := []int{55, 44, 32, 21, 14, 5, 4, 4, 4, 4, 3, 2, 1}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSortOptimized(data)
	}
}

func BenchmarkBubbleSortOptimizedSwapCheck(b *testing.B) {
	data := []int{55, 44, 32, 21, 14, 5, 4, 4, 4, 4, 3, 2, 1}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSortOptimizedSwapCheck(data)
	}
}

func TestBubbleSort(t *testing.T) {
	type args struct {
		data []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "reverse sort",
			args: args{
				data: []int{55, 44, 32, 21, 14, 5, 4, 3, 2, 1},
			},
			want: []int{1, 2, 3, 4, 5, 14, 21, 32, 44, 55},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BubbleSort(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BubbleSort() = %v, want %v", got, tt.want)
			}
		})
	}
}
