package algo

func mergeSort(data []int) []int {
	if len(data) < 2 {
		return data
	}

	one := mergeSort(data[:len(data)/2])
	two := mergeSort(data[len(data)/2:])

	return merge(one, two)
}

func merge(a, b []int) []int {
	result := make([]int, 0)
	var i, j int

	for i < len(a) && j < len(b) {
		if a[i] < b[j] {
			result = append(result, a[i])
			i++
		} else {
			result = append(result, b[j])
			j++
		}
	}

	for i < len(a) {
		result = append(result, a[i])
		i++
	}

	for j < len(b) {
		result = append(result, b[j])
		j++
	}

	return result
}
