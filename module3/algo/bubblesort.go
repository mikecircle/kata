package algo

func BubbleSort(data []int) []int {
	for range data {
		for j := 0; j < len(data); j++ {
			if j == len(data)-1 {
				break
			}
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
			}
		}
	}
	return data
}

func BubbleSortOptimized(data []int) []int {
	for i := range data {
		for j := 0; j < len(data)-i-1; j++ {
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
			}
		}
	}
	return data
}

func BubbleSortOptimizedSwapCheck(data []int) []int {
	swapped := true
	i := 0
	for swapped {
		swapped = false
		for j := 0; j < len(data)-i; j++ {
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
				swapped = true
			}
			i++
		}
	}
	return data
}
