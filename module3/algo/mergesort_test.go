package algo

import (
	"reflect"
	"sort"
	"testing"
)

func Test_mergeSort(t *testing.T) {
	data := randomData(5, 1000)
	sorted := append([]int{}, data()...)
	sort.Ints(sorted)
	type args struct {
		data []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "reverse sorting",
			args: args{[]int{99, 88, 77, 55, 44, 32, 24, 15, 5, 4, 3, 2, 1}},
			want: []int{1, 2, 3, 4, 5, 15, 24, 32, 44, 55, 77, 88, 99},
		},
		{
			name: "random array",
			args: args{data: data()},
			want: sorted,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := mergeSort(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("mergeSort() = %v, want %v", got, tt.want)
			}
		})
	}
}
