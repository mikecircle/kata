package algo

import (
	"math/rand"
	"time"
)

func randomData(n, max int) func() []int {
	var data []int
	return func() []int {
		if data != nil {
			return data
		}
		rand.Seed(time.Now().UnixNano())
		for i := 0; i < n; i++ {
			data = append(data, rand.Intn(max))
		}
		return data
	}
}

func QuickSort(data []int) []int {
	if len(data) <= 1 {
		return data
	}

	median := data[rand.Intn(len(data))]

	lowPart := make([]int, 0, len(data))
	highPart := make([]int, 0, len(data))
	middlePart := make([]int, 0, len(data))

	for _, i := range data {
		switch {
		case i < median:
			lowPart = append(lowPart, i)
		case i == median:
			middlePart = append(middlePart, i)
		case i > median:
			highPart = append(highPart, i)
		}
	}
	lowPart = QuickSort(lowPart)
	highPart = QuickSort(highPart)
	lowPart = append(lowPart, middlePart...)
	lowPart = append(lowPart, highPart...)

	return lowPart
}
