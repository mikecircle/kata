package main

import (
	"gitlab.com/mikecircle/kata/module3/leetcode/easy"
	"gitlab.com/mikecircle/kata/module3/leetcode/medium"
)

func main() {
	_ = easy.Tribonacci(37)
	_ = easy.GetConcatenation([]int{1, 2, 3, 4, 5})
	_ = easy.ConvertTemperature(12.00)
	_ = easy.BuildArray([]int{0, 2, 1, 5, 3, 4})
	_ = easy.NumberOfMatches(1)
	_ = easy.UniqueMorseRepresentations([]string{"gin", "zen", "gig", "msg"})
	_ = easy.DefangIPaddr("1.1.1.1")
	_ = easy.FindKthPositive([]int{2, 3, 4, 7, 11}, 5)
	_ = easy.FinalValueAfterOperations([]string{"--X", "X++", "X++"})
	_ = easy.Shuffle([]int{2, 5, 1, 3, 4, 7}, 3)
	_ = easy.RunningSum([]int{3, 1, 2, 10, 1})
	_ = easy.NumIdenticalPairs([]int{1, 2, 3, 1, 1, 3})
	_ = easy.NumJewelsInStones("aA", "aAAbbbb")
	_ = easy.MaximumWealth([][]int{{1, 2, 3}, {3, 2, 1}})
	test := easy.Constructor(1, 2, 3)
	test.AddCar(2)
	_ = easy.SmallestEvenMultiple(5)
	_ = easy.MostWordsFound([]string{"please wait", "continue to fight", "continue to win"})
	_ = easy.DifferenceOfSum([]int{1, 15, 6, 3})
	_ = easy.MinimumSum(2932)
	_ = easy.KidsWithCandies([]int{2, 3, 5, 1, 3}, 3)
	_ = easy.SubtractProductAndSum(234)
	_ = easy.SmallerNumbersThanCurrent([]int{8, 1, 2, 2, 3})
	_ = easy.Interpret("G()(al)")
	_ = easy.Decode([]int{6, 2, 7, 3}, 4)
	_ = easy.CreateTargetArray([]int{0, 1, 2, 3, 4}, []int{0, 1, 2, 2, 1})
	_ = easy.DecompressRLElist([]int{1, 2, 3, 4})
	_ = easy.BalancedStringSplit("RLRRLLRLRL")
	medium.MakeTree([]int{1, 2, 3, 4, 5, 6, 7, 8})
	_ = medium.SortTheStudents([][]int{{10, 6, 9, 1}, {7, 5, 11, 2}, {4, 8, 3, 15}}, 2)
	medium.DeepestLeavesSum(&medium.TreeNode{})
	medium.MergeNodes(medium.NewList([]int{0, 3, 1, 0, 4, 5, 2, 0}))
	medium.BstToGst(medium.MakeTestTree())
	medium.MakeAndExecutePairSum([]int{5, 4, 2, 1})
	medium.ConstructMaximumBinaryTree([]int{3, 2, 1, 6, 0, 5})
	medium.MakeAndExecuteBalanceBST()
	medium.FindSmallestSetOfVertices(6, [][]int{{0, 1}, {0, 2}, {2, 5}, {3, 4}, {4, 2}})
	medium.CheckArithmeticSubarrays([]int{4, 6, 5, 9, 3, 7}, []int{0, 0, 2}, []int{2, 3, 5})
	medium.NumTilePossibilities("AAABBC")
	medium.MakeAndRunMergeInBetween([]int{0, 1, 2, 3, 4, 5, 6}, 2, 5, []int{1000000, 1000001, 1000002, 1000003, 1000004})
	medium.MaxSum([][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}})
	_ = medium.MinPartitions("3895")
	medium.GroupThePeople([]int{3, 1, 2, 1, 3, 2, 3})
}
