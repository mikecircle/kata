package medium

func (n *TreeNode) InsertR(v int) *TreeNode {
	n.Right = &TreeNode{
		Val: v,
	}
	return n.Right
}

func (n *TreeNode) InsertL(v int) *TreeNode {
	n.Left = &TreeNode{
		Val: v,
	}
	return n.Left
}

//func (n *TreeNode) printTreeTraversal() {
//	if n.Left != nil {
//		n.Left.printTreeTraversal()
//	}
//	fmt.Print(n.Val, ", ")
//	if n.Right != nil {
//		n.Right.printTreeTraversal()
//	}
//}

func MakeTestTree() *TreeNode {
	tn := &TreeNode{
		Val: 4,
	}
	tnR := tn.InsertR(6)
	tnR.InsertR(7)
	tnR.InsertL(5)
	tnL := tn.InsertL(1)
	tnL.InsertR(2)
	tnL.InsertL(0)
	//tn.printTreeTraversal()
	return tn
}

func BstToGst(root *TreeNode) *TreeNode {
	var dfs func(node *TreeNode, greaterSum int) int
	dfs = func(node *TreeNode, greaterSum int) int {
		if node == nil {
			return 0
		}
		r := dfs(node.Right, greaterSum)
		tmp := node.Val
		node.Val += r + greaterSum
		return r + tmp + dfs(node.Left, node.Val)
	}
	dfs(root, 0)
	//fmt.Println()
	//root.printTreeTraversal()
	return root
}
