package medium

func MakeAndRunMergeInBetween(l1 []int, a int, b int, l2 []int) {
	list1, list2 := &ListNode{}, &ListNode{}
	head1, head2 := list1, list2
	for i, j := range l1 {
		if i == 0 {
			list1.Val = j
			continue
		}
		list1.Next = &ListNode{
			Val: j,
		}
		list1 = list1.Next
	}
	for i, j := range l2 {
		if i == 0 {
			list2.Val = j
			continue
		}
		list2.Next = &ListNode{
			Val: j,
		}
		list2 = list2.Next
	}

	MergeInBetween(head1, a, b, head2)
}

func MergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	var bthNode *ListNode
	headOne := list1
	for i := 0; i < b; i++ {
		list1 = list1.Next
	}
	bthNode = list1.Next
	list1 = headOne
	for i := 1; i < a; i++ {
		list1 = list1.Next
	}
	list1 = list2
	for list2.Next != nil {
		list2 = list2.Next
	}
	list2.Next = bthNode

	return headOne
}
