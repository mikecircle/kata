package medium

func MakeAndExecuteBalanceBST() {
	//root = [1,null,2,null,3,null,4,null,null]
	var four, two, zero *TreeNode
	zero = &TreeNode{Val: 0}
	four = &TreeNode{Val: 4, Left: nil, Right: nil}
	three := &TreeNode{Val: 3, Left: nil, Right: four}
	two = &TreeNode{Val: 2, Left: nil, Right: three}
	head := &TreeNode{Val: 1, Left: zero, Right: two}

	BalanceBST(head)
}

func createBBST(slice []int, l, r int) *TreeNode {
	if l > r {
		return nil
	}
	m := (l + r) / 2
	root := &TreeNode{Val: slice[m]}
	root.Left = createBBST(slice, l, m-1)
	root.Right = createBBST(slice, m+1, r)
	return root
}

func BalanceBST(root *TreeNode) *TreeNode {
	var sortedArray []int
	inOrderSlice(root, &sortedArray)
	root = createBBST(sortedArray, 0, len(sortedArray)-1)
	return root
}

func inOrderSlice(node *TreeNode, slice *[]int) {
	if node == nil {
		return
	}
	inOrderSlice(node.Left, slice)
	*slice = append(*slice, node.Val)
	inOrderSlice(node.Right, slice)
}
