package medium

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func MakeTree(s []int) {
	tree := &TreeNode{Val: s[0]}
	for _, v := range s {
		tree.Insert(v)
	}
	//fmt.Println(DeepestLeavesSum(tree))
}

func (n *TreeNode) Insert(k int) {
	if n.Val < k {
		if n.Left == nil {
			n.Left = &TreeNode{Val: k}
		} else {
			n.Left.Insert(k)
		}
	}
	if n.Val > k {
		if n.Right == nil {
			n.Right = &TreeNode{Val: k}
		} else {
			n.Right.Insert(k)
		}
	}
}

func DeepestLeavesSum(root *TreeNode) int {
	sum := 0
	// base case
	if root == nil {
		return sum
	}

	queue := []*TreeNode{root}

	for len(queue) > 0 {
		l := len(queue)

		// Reset sum to 0 until we hit the last leaves
		sum = 0
		for i := 0; i < l; i++ {
			node := queue[i]

			// Keep the sum for all the nodes in a row
			// Note: for deepest leaves, this will be the answer
			sum += node.Val

			if node.Left != nil {
				queue = append(queue, node.Left)
			}

			if node.Right != nil {
				queue = append(queue, node.Right)
			}
		}
		queue = queue[l:]
	}
	return sum
}
