package medium

func MakeAndRunRemoveLeafNodes() {
	//[1,2,3,2,null,2,4]
	node := &TreeNode{Val: 1}
	node.Left = &TreeNode{Val: 2}
	node.Right = &TreeNode{Val: 3}
	node.Left.Left = &TreeNode{Val: 2}
	node.Right.Left = &TreeNode{Val: 2}
	node.Right.Right = &TreeNode{Val: 4}
	RemoveLeafNodes(node, 2)
}

func RemoveLeafNodes(root *TreeNode, target int) *TreeNode {
	if root.Left != nil {
		root.Left = RemoveLeafNodes(root.Left, target)
	}
	if root.Right != nil {
		root.Right = RemoveLeafNodes(root.Right, target)
	}
	if root.Left == nil && root.Right == nil && root.Val == target {
		return nil
	}
	return root
}
