package medium

func MaxSum(grid [][]int) int {
	m := len(grid)           //3
	n := len(grid[0])        //3
	for i := 0; i < m; i++ { //y = 1
		for j := 1; j < n; j++ { //j = 4
			grid[i][j] = grid[i][j] + grid[i][j-1] //grid[0][1] =
		}
	}
	ans := 0
	for y := 2; y < m; y++ { //y = 2
		sum := 0
		for x := 2; x < n; x++ { //x = 2
			sum = grid[y][x] + grid[y-2][x] + grid[y-1][x-1] - grid[y-1][x-2] //2 2
			if x > 2 {
				sum -= grid[y][x-3] + grid[y-2][x-3]
			}
			if sum > ans {
				ans = sum
			}
		}
	}
	return ans
}
