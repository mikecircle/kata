package medium

func ConstructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	maxV, maxInd := -1, -1

	for i, v := range nums {
		if v > maxV {
			maxV = v
			maxInd = i
		}
	}

	root := &TreeNode{
		Val: maxV,
	}
	root.Left = ConstructMaximumBinaryTree(nums[:maxInd])
	root.Right = ConstructMaximumBinaryTree(nums[maxInd+1:])
	return root
}
