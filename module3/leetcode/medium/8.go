package medium

func FindSmallestSetOfVertices(n int, edges [][]int) []int {
	ins := make(map[int]bool)
	var result []int
	for _, v := range edges {
		ins[v[1]] = true
	}
	for i := 0; i < n; i++ {
		if !ins[i] {
			result = append(result, i)
		}
	}
	return result
}
