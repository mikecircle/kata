package medium

func MinPartitions(n string) int {
	maxDigit := 0
	for i := 0; i < len(n); i++ {
		digit := int(n[i] - '0')
		if digit > maxDigit {
			maxDigit = digit
		}
	}
	return maxDigit
}
