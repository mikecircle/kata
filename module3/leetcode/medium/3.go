package medium

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func (n *ListNode) Insert(i int) {
	n.Next = &ListNode{
		Val: i,
	}
}

func NewList(vals []int) *ListNode {
	node := &ListNode{
		Val: vals[0],
	}
	headNode := node
	for i, v := range vals {
		if i == 0 {
			continue
		}
		node.Insert(v)
		node = node.Next
	}
	return headNode
}

func (n *ListNode) GetLength() int {
	l := 1
	node := n
	for node.Next != nil {
		l++
		node = node.Next
	}
	return l
}

func (n *ListNode) PrintList() {
	head := n
	for i := 0; i < n.GetLength(); i++ {
		fmt.Printf("%v ", head.Val)
		head = head.Next
	}
}

func MergeNodes(head *ListNode) *ListNode {
	var sum int

	head = head.Next
	var node, nodeHead *ListNode
	for head != nil {
		sum += head.Val

		if head.Val == 0 {
			if node == nil {
				node = &ListNode{Val: sum}
				nodeHead = node
			} else {
				node.Next = &ListNode{Val: sum}
				node = node.Next
			}
			sum = 0
		}
		head = head.Next
	}

	//nodeHead.PrintList()
	return nodeHead
}
