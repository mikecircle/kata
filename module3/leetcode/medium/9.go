package medium

import (
	"sort"
)

func CheckArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	result := make([]bool, len(l))
	for i := range l {
		result[i] = isArithmetic(nums[l[i] : r[i]+1])
	}
	return result
}

func isArithmetic(nums []int) bool {
	sorted := make([]int, len(nums))
	copy(sorted, nums)
	sort.Ints(sorted)
	a := true
	diff := sorted[1] - sorted[0]
	for i := 0; i < len(nums)-1; i++ {
		if sorted[i+1]-sorted[i] != diff {
			a = false
		}
	}
	return a
}
