package medium

type LList struct {
	Val  int
	Next *LList
}

func MakeAndExecutePairSum(v []int) {
	list := &LList{}
	head := list
	for i, j := range v {
		if i == 0 {
			list.Val = j
			continue
		}
		list.Next = &LList{
			Val: j,
		}
		list = list.Next
	}
	PairSum(head)
}

func PairSum(head *LList) int {
	fast, slow := head, head
	max := 0
	var arr []int
	i := 0

	for slow != nil {
		if fast != nil {
			arr = append(arr, slow.Val)
			fast = fast.Next.Next
		} else {
			temp := arr[len(arr)-1-i] + slow.Val
			if temp > max {
				max = temp
			}
			i++
		}
		slow = slow.Next
	}
	return max
}
