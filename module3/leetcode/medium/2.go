package medium

func SortTheStudents(score [][]int, k int) [][]int {
	kthResults := make([]int, len(score))
	result := score
	for i, v := range score {
		for j, p := range v {
			if j == k {
				kthResults[i] = p
			}
		}
	}

	for i := range kthResults {
		for j := 0; j < len(kthResults)-i-1; j++ {
			if kthResults[j] < kthResults[j+1] {
				kthResults[j], kthResults[j+1] = kthResults[j+1], kthResults[j]
				result[j], result[j+1] = result[j+1], result[j]
			}
		}
	}
	return result
}
