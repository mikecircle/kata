package easy

func NumIdenticalPairs(nums []int) int {
	count := 0
	for i := range nums {
		for j := range nums {
			if nums[i] == nums[j] && i < j {
				count++
			}
		}
	}
	return count
}
