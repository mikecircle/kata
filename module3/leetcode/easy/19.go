package easy

import (
	"sort"
)

func MinimumSum(num int) int {
	var slice []int
	for num > 0 {
		slice = append(slice, num%10)
		num /= 10
	}
	sort.Ints(slice)
	num1 := slice[0]*10 + slice[2]
	num2 := slice[1]*10 + slice[3]
	return num1 + num2
}
