package easy

func CreateTargetArray(nums []int, index []int) []int {
	var target []int
	for i, v := range nums {
		target = insert(target, index[i], v)
	}
	return target
}

func insert(s []int, idx int, value int) []int {
	if idx == len(s) {
		return append(s, value)
	}
	temp := make([]int, len(s[idx:]))
	copy(temp, s[idx:])
	s = s[:idx+1]
	s[idx] = value
	temp = append(s, temp...)
	return temp
}
