package easy

import (
	"strings"
)

func NumJewelsInStones(jewels string, stones string) int {
	jewelSlice := strings.Split(jewels, "")
	count := 0
	for _, v := range jewelSlice {
		count += strings.Count(stones, v)
	}
	return count
}
