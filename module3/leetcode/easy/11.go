package easy

func RunningSum(nums []int) []int {
	sum := 0
	sumSlice := make([]int, len(nums))

	for i, v := range nums {
		sum += v
		sumSlice[i] = sum
	}
	return sumSlice
}
