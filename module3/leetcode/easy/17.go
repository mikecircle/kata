package easy

import "strings"

func MostWordsFound(sentences []string) int {
	maxCount := 0
	for _, v := range sentences {
		wordCount := len(strings.Split(v, " "))
		if wordCount > maxCount {
			maxCount = wordCount
		}
	}
	return maxCount
}
