package easy

func KidsWithCandies(candies []int, extraCandies int) []bool {
	result := make([]bool, len(candies))
	maxCandies := 0
	for i := range candies {
		if candies[i] > maxCandies {
			maxCandies = candies[i]
		}
	}
	for i := range candies {
		if candies[i]+extraCandies >= maxCandies {
			result[i] = true
		}
	}
	return result
}
