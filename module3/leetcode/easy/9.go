package easy

func FinalValueAfterOperations(operations []string) int {
	x := 0
	for i := range operations {
		if operations[i] == "++X" || operations[i] == "X++" {
			x++
		} else {
			x--
		}
	}
	return x
}
