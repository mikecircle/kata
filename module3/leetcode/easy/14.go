package easy

func MaximumWealth(accounts [][]int) int {
	maxWealth := 0

	for _, money := range accounts {
		total := 0
		for _, v := range money {
			total += v
		}
		if total > maxWealth {
			maxWealth = total
		}
	}
	return maxWealth
}
