package easy

//[]int{2, 5, 1, 3, 4, 7}, 3)

func Shuffle(nums []int, n int) []int {
	var result []int
	ri, le := nums[n:], nums[:n]
	for i := 0; i < len(le); i++ {
		result = append(result, le[i])
		result = append(result, ri[i])
	}
	return result
}
