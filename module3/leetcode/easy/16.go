package easy

func SmallestEvenMultiple(n int) int {
	for i := 1; ; i++ {
		if i%2 == 0 && i%n == 0 {
			return i
		}
	}
}
