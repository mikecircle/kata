package easy

func DifferenceOfSum(nums []int) int {
	numSum := 0
	digitSum := 0
	for _, v := range nums {
		numSum += v

		for v > 0 {
			digitSum += v % 10
			v /= 10
		}
	}
	result := numSum - digitSum
	if result < 0 {
		return -1 * result
	}
	return result
}
