package easy

func NumberOfMatches(n int) int {
	matches := 0
	if n == 1 {
		return 0
	}
	for n != 2 {
		if n%2 == 0 {
			matches += n / 2
			n /= 2
		} else if n%2 != 0 {
			matches += (n - 1) / 2
			n = (n-1)/2 + 1
		}
	}
	return matches + 1
}
