package easy

type ParkingSystem struct {
	big    int
	medium int
	small  int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	park := ParkingSystem{
		big:    big,
		medium: medium,
		small:  small,
	}
	return park
}

func (this *ParkingSystem) AddCar(carType int) bool {
	result := false
	switch {
	case carType == 1 && this.big != 0:
		result = true
		this.big--
	case carType == 2 && this.medium != 0:
		result = true
		this.medium--
	case carType == 3 && this.small != 0:
		result = true
		this.small--
	}
	return result
}
