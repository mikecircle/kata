package easy

func Tribonacci(n int) int {
	trib := make([]int, 100)
	trib[0], trib[1], trib[2] = 0, 1, 1
	var result int
	if n < 3 {
		result = trib[n]
	}
	for i := 3; i <= n; i++ {
		trib[i] = trib[i-3] + trib[i-2] + trib[i-1]
		//fmt.Print("i = ", i)
		//fmt.Print(" ", trib[i], " \n")
		result = trib[i]
	}
	return result
}
