package easy

func GetConcatenation(nums []int) []int {
	newArray := make([]int, len(nums)*2)
	for i := range nums {
		newArray[i], newArray[i+len(nums)] = nums[i], nums[i]
	}
	return newArray
}
