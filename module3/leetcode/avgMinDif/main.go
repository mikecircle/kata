package main

import (
	"fmt"
	"math"
)

func main() {
	nums := []int{2, 5, 3, 9, 5, 3}
	fmt.Println(minimumAverageDifference(nums))
}

func minimumAverageDifference(nums []int) int {
	var avgDiff, minIndex, minAvgDiff int
	var divValueOne, divValueTwo int
	var firstHalfSum, secondHalfSum int
	minAvgDiff = math.MaxInt
	for _, v := range nums {
		secondHalfSum += v
	}
	divValueTwo = len(nums)
	for i := 0; i < len(nums); i++ {
		firstHalfSum += nums[i]
		secondHalfSum -= nums[i]
		divValueTwo--
		divValueOne++

		if divValueTwo == 0 {
			divValueTwo++
		}

		avgDiff = firstHalfSum/divValueOne - secondHalfSum/divValueTwo
		if avgDiff < 0 {
			avgDiff *= -1
		}
		if avgDiff < minAvgDiff {
			minAvgDiff = avgDiff
			minIndex = i
		}

	}
	return minIndex
}
