package repository

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File io.ReadWriteSeeker
}

func NewUserRepository(file io.ReadWriteSeeker) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func truncate(rws io.ReadWriteSeeker, size int64) error {
	type Truncater interface {
		Truncate(size int64) error
	}
	t, ok := rws.(Truncater)
	if !ok {
		return fmt.Errorf("truncate: unable to truncate")
	}
	return t.Truncate(size)
}

func (r *UserRepository) Open() (users []User, fileContents []byte, err error) {
	_, err = r.File.Seek(0, 0)
	if err != nil {
		return users, fileContents, err
	}
	users = make([]User, 0)
	fileContents, err = io.ReadAll(r.File)
	if err != nil {
		return users, fileContents, err
	}
	return users, fileContents, err
}

func (r *UserRepository) Save(record interface{}) error {
	// logic to write a record as a JSON object to a file at r.FilePath
	users, fileContents, err := r.Open()
	if err != nil {
		return err
	}
	user, ok := record.(User)
	idCheck, _ := r.Find(user.ID)
	switch {
	case !ok:
		return errors.New("wrong type of input")
	case user.Name == "":
		return errors.New("name can't be empty")
	case idCheck != nil:
		return fmt.Errorf("user with id %v already exists", user.ID)
	}
	if len(fileContents) != 0 {
		err = json.Unmarshal(fileContents, &users)
		if err != nil {
			return err
		}
		err = truncate(r.File, 0)
		if err != nil {
			return err
		}
		_, err = r.File.Seek(0, 0)
		if err != nil {
			return err
		}
	}
	users = append(users, user)
	marshalled, err := json.Marshal(users)
	if err != nil {
		return err
	}
	_, err = r.File.Write(marshalled)
	if err != nil {
		return err
	}
	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	users, fileContents, err := r.Open()
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(fileContents, &users)
	if err != nil {
		return nil, err
	}
	for _, v := range users {
		if v.ID == id {
			return v, nil
		}
	}
	return nil, errors.New("user is not found")
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	// logic to read all JSON objects from a file at r.FilePath and return a slice of records
	users, fileContents, err := r.Open()
	switch {
	case err != nil:
		return nil, err
	case len(fileContents) == 0:
		return nil, errors.New("file is empty")
	}
	err = json.Unmarshal(fileContents, &users)
	if err != nil {
		return nil, err
	}
	output := make([]interface{}, len(users))
	for i, s := range users {
		output[i] = s
	}
	return output, err
}
