package repository

import (
	"os"
	"reflect"
	"testing"
)

func TestUserRepository_Save(t *testing.T) {
	repoFile, _ := os.OpenFile("./temp.json", os.O_CREATE|os.O_RDWR, os.ModePerm)
	repo := NewUserRepository(repoFile)
	defer func() {
		_ = repoFile.Close()
		_ = os.Remove(repoFile.Name())
	}()

	tests := []struct {
		name    string
		fields  *UserRepository
		args    interface{}
		want    string
		wantErr bool
	}{
		{
			name:    "add first user",
			fields:  repo,
			args:    User{Name: "The first user", ID: 0},
			want:    "[{\"id\":0,\"name\":\"The first user\"}]",
			wantErr: false,
		},
		{
			name:    "add second user",
			fields:  repo,
			args:    User{Name: "The second user", ID: 1},
			want:    "[{\"id\":0,\"name\":\"The first user\"},{\"id\":1,\"name\":\"The second user\"}]",
			wantErr: false,
		},
		{
			name:    "add user with existing id",
			fields:  repo,
			args:    User{Name: "The existing user", ID: 1},
			want:    "[{\"id\":0,\"name\":\"The first user\"},{\"id\":1,\"name\":\"The second user\"}]",
			wantErr: true,
		},
		{
			name:    "add user without a name",
			fields:  repo,
			args:    User{ID: 3},
			want:    "[{\"id\":0,\"name\":\"The first user\"},{\"id\":1,\"name\":\"The second user\"}]",
			wantErr: true,
		},
		{
			name:    "send unsupported type",
			fields:  repo,
			args:    "User{ID: 3}",
			want:    "[{\"id\":0,\"name\":\"The first user\"},{\"id\":1,\"name\":\"The second user\"}]",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			if err := r.Save(tt.args); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
			fileContent, _ := os.ReadFile("./temp.json")
			if string(fileContent) != tt.want {
				t.Errorf("Save() file = %v, want %v", string(fileContent), tt.want)
			}
		})
	}
}

func TestUserRepository_Find(t *testing.T) {
	repoFile, _ := os.OpenFile("./temp.json", os.O_CREATE|os.O_RDWR, os.ModePerm)
	repo := NewUserRepository(repoFile)
	defer func() {
		_ = repoFile.Close()
		_ = os.Remove(repoFile.Name())
	}()

	testUsers := []User{
		{Name: "Dimas", ID: 0},
		{Name: "Vladimir", ID: 1},
	}
	for _, v := range testUsers {
		_ = repo.Save(v)
	}

	tests := []struct {
		name    string
		fields  *UserRepository
		args    int
		want    interface{}
		wantErr bool
	}{
		{
			name:    "finding existing user",
			fields:  repo,
			args:    0,
			want:    testUsers[0],
			wantErr: false,
		},
		{
			name:    "finding not existing user",
			fields:  repo,
			args:    1024,
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			got, err := r.Find(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Find() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	repoFile, _ := os.OpenFile("./temp.json", os.O_CREATE|os.O_RDWR, os.ModePerm)
	emptyFile, _ := os.OpenFile("./temp_empty.json", os.O_CREATE|os.O_RDWR, os.ModePerm)
	repo := NewUserRepository(repoFile)
	emptyRepo := NewUserRepository(emptyFile)
	defer func() {
		_ = repoFile.Close()
		_ = emptyFile.Close()
		_ = os.Remove(repoFile.Name())
		_ = os.Remove(emptyFile.Name())
	}()

	testUsers := []User{
		{Name: "Dimas", ID: 0},
		{Name: "Vladimir", ID: 1},
	}
	for _, v := range testUsers {
		_ = repo.Save(v)
	}
	testOutput := make([]interface{}, len(testUsers))
	for i, s := range testUsers {
		testOutput[i] = s
	}

	tests := []struct {
		name    string
		fields  *UserRepository
		want    []interface{}
		wantErr bool
	}{
		{
			name:    "right output",
			fields:  repo,
			want:    testOutput,
			wantErr: false,
		},
		{
			name:    "wrong file",
			fields:  emptyRepo,
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			got, err := r.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("FindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindAll() got = %v, want %v", got, tt.want)
			}
		})
	}
}
