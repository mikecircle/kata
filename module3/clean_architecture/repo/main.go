package main

import (
	"fmt"
	"os"

	"gitlab.com/mikecircle/kata/module3/clean_architecture/repo/repository"
)

func main() {
	repoFile, _ := os.OpenFile("/home/shroomov/go/src/gitlab.com/mikecircle/kata/module3/clean_architecture/repo/repository.json", os.O_CREATE|os.O_RDWR, os.ModePerm)
	repo := repository.NewUserRepository(repoFile)
	//user := repository.User{Name: "The second user", ID: 1}
	res, err := repo.FindAll()
	fmt.Println(res, err)
}
