package service

import (
	. "gitlab.com/mikecircle/kata/module3/clean_architecture/service/model"
	"gitlab.com/mikecircle/kata/module3/clean_architecture/service/repo"
)

type TodoService interface {
	ListTodos() ([]Todo, error)
	CreateTodo(title string) error
	CompleteTodo(todo Todo) error
	RemoveTodo(todo Todo) error
}

type TodoRepository interface {
	GetTasks() ([]repo.Task, error)
	GetTask(id int) (repo.Task, error)
	CreateTask(task repo.Task) (repo.Task, error)
	UpdateTask(task repo.Task) (repo.Task, error)
	DeleteTask(id int) error
}

type todoService struct {
	repository TodoRepository
}

func NewTodoService(r TodoRepository) *todoService {
	return &todoService{repository: r}
}

func (s *todoService) ListTodos() ([]Todo, error) {
	tasks, err := s.repository.GetTasks()
	if err != nil {
		return nil, err
	}
	todoList := make([]Todo, len(tasks))
	for i, v := range tasks {
		todoList[i].Status = v.Status
		todoList[i].Description = v.Description
		todoList[i].Title = v.Title
		todoList[i].ID = v.ID
	}
	return todoList, nil
}

func (s *todoService) CreateTodo(title string) error {
	task := repo.Task{Title: title}
	_, err := s.repository.CreateTask(task)
	if err != nil {
		return err
	}
	return nil
}

func (s *todoService) CompleteTodo(todo Todo) error {
	task := repo.Task{ID: todo.ID, Title: todo.Title, Description: todo.Description, Status: "Completed"}
	_, err := s.repository.UpdateTask(task)
	if err != nil {
		return err
	}
	return nil
}

func (s *todoService) RemoveTodo(todo Todo) error {
	err := s.repository.DeleteTask(todo.ID)
	if err != nil {
		return err
	}
	return nil
}
