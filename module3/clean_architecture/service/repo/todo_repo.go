package repo

import (
	"encoding/json"
	"errors"
	"io"
	"os"
)

// Repository layer

// Task represents a to-do task
type Task struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Status      string `json:"status"`
}

// TaskRepository is a repository interface for tasks
type TaskRepository interface {
	GetTasks() ([]Task, error)
	GetTask(id int) (Task, error)
	CreateTask(task Task) (Task, error)
	UpdateTask(task Task) (Task, error)
	DeleteTask(id int) error
	SaveTasks(tasks []Task) error
}

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
	FilePath string
}

func NewFileTaskRepository(file string) *FileTaskRepository {
	return &FileTaskRepository{FilePath: file}
}

func (repo *FileTaskRepository) DeleteTask(id int) error {

	task, err := repo.GetTask(id)
	if err != nil {
		return err
	}
	task.Status = "Deleted"
	_, err = repo.UpdateTask(task)
	if err != nil {
		return err
	}

	return nil
}

func (repo *FileTaskRepository) SaveTasks(tasks []Task) error {
	file, err := os.OpenFile(repo.FilePath, os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer func(open *os.File) {
		err := open.Close()
		if err != nil {
			panic(err)
		}
	}(file)
	text, err := json.Marshal(tasks)
	if err != nil {
		return err
	}
	_, err = file.Write(text)
	if err != nil {
		return err
	}
	return nil
}

// GetTasks returns all tasks from the repository
func (repo *FileTaskRepository) GetTasks() ([]Task, error) {
	var tasks []Task

	file, err := os.Open(repo.FilePath)
	if err != nil {
		return nil, err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			panic(err)
		}
	}(file)

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(content, &tasks); err != nil {
		return nil, err
	}

	return tasks, nil
}

// GetTask returns a single task by its ID
func (repo *FileTaskRepository) GetTask(id int) (Task, error) {
	var task Task
	tasks, err := repo.GetTasks()
	if id > len(tasks) || id < 0 {
		return task, errors.New("failed to GetTask: id is out of range")
	}
	//fmt.Println("GETTING TASK", id, tasks)
	if err != nil {
		return task, err
	}

	for _, t := range tasks {
		if t.ID == id {
			task = t
			return task, nil
		}
	}
	return task, nil
}

// CreateTask adds a new task to the repository
func (repo *FileTaskRepository) CreateTask(task Task) (Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil && len(tasks) != 0 {
		return task, err
	}

	task.ID = len(tasks) + 1
	tasks = append(tasks, task)

	if err := repo.SaveTasks(tasks); err != nil {
		return task, err
	}

	return task, nil
}

// UpdateTask updates an existing task in the repository
func (repo *FileTaskRepository) UpdateTask(task Task) (Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	for i, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
	}

	if err := repo.SaveTasks(tasks); err != nil {
		return task, err
	}

	return task, nil
}
