package main

import (
	"fmt"
	"os"

	"gitlab.com/mikecircle/kata/module3/clean_architecture/service/repo"
	"gitlab.com/mikecircle/kata/module3/clean_architecture/service/service"
)

func main() {
	task := repo.Task{Title: "test task", Description: "desc", Status: "Not completed"}
	_ = task
	file, _ := os.Create("/home/shroomov/go/src/gitlab.com/mikecircle/kata/repository.json")
	taskRep := repo.NewFileTaskRepository(file.Name())
	todo := service.NewTodoService(taskRep)
	err := todo.CreateTodo("New Task")
	if err != nil {
		return
	}
	err = todo.CreateTodo("New Task Two")
	if err != nil {
		return
	}
	err = todo.CreateTodo("New Task Three")
	if err != nil {
		return
	}
	list, _ := todo.ListTodos()
	_ = todo.CreateTodo("test")
	_ = todo.RemoveTodo(list[0])
	fmt.Println(todo.ListTodos())
	_ = todo.CompleteTodo(list[0])
	fmt.Println(todo.ListTodos())
	_ = todo.CompleteTodo(list[1])
	fmt.Println(todo.ListTodos())
}
