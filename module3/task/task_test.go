package main

import (
	"reflect"
	"testing"
)

func TestDoubleLinkedList_Reverse(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *DoubleLinkedList
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Reverse(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Reverse() = %v, want %v", got, tt.want)
			}
		})
	}
}
