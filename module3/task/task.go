package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/brianvoe/gofakeit/v6"

	_ "github.com/brianvoe/gofakeit/v6"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	Pop() *Node
	Shift() *Node
	LoadData(path string) error
	PrintList()
	Append(item Commit)
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	SearchUUID(uuID string) *Node
	Search(name string) *Node
	Index() (int, error)
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	var commits []Commit
	f, _ := os.ReadFile(path)
	err := json.Unmarshal(f, &commits)
	if err != nil {
		panic(err)
	}
	commits = QuickSort(commits)
	for _, v := range commits {
		d.Append(v)
	}
	d.curr = d.head
	return nil
}

func QuickSort(c []Commit) []Commit {
	if len(c) <= 1 {
		return c
	}
	med := c[rand.Intn(len(c))].Date.Unix()

	lo := make([]Commit, 0, len(c))
	hi := make([]Commit, 0, len(c))
	mid := make([]Commit, 0, len(c))

	for _, v := range c {
		switch {
		case v.Date.Unix() < med:
			lo = append(lo, v)
		case v.Date.Unix() == med:
			mid = append(mid, v)
		case v.Date.Unix() > med:
			hi = append(hi, v)
		}
	}
	lo = QuickSort(lo)
	hi = QuickSort(hi)

	lo = append(lo, mid...)
	lo = append(lo, hi...)
	return lo
}

func (d *DoubleLinkedList) Append(item Commit) {
	node := &Node{data: &item, prev: nil, next: nil}
	if d.head == nil {
		d.head = node
		d.tail = node
	} else {
		currentNode := d.head
		for currentNode.next != nil {
			currentNode = currentNode.next
		}
		node.prev = currentNode
		d.curr = d.tail
		d.curr.next = node
		d.tail = node
	}
	d.len++
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	fmt.Println("CURRENT NODE: ", d.curr.data)
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr.next == nil {
		d.curr = d.head
		fmt.Println("NO NEXT NODE\nMOVING TO HEAD")
		d.Current()
		return d.curr
	}
	d.curr = d.curr.next
	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr.prev == nil {
		fmt.Println("NO PREVIOUS NODE")
		d.Current()
		return d.curr
	}
	d.curr = d.curr.prev
	return d.curr
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	fmt.Println("STARTING INSERT")
	if n >= d.len || n < 0 {
		return errors.New("index out of range")
	}

	node := &Node{data: &c}
	d.curr = d.head
	for {
		idx, _ := d.Index()
		if idx == n {
			if d.curr == d.tail {
				d.Append(c)
				return nil
			}
			node.prev = d.curr
			node.next = d.curr.next
			node.prev.next = node
			node.next.prev = node
			d.len++
			return nil
		}
		d.Next()
	}
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if n >= d.len || n < 0 {
		return errors.New("index out of range")
	}
	for {
		idx, _ := d.Index()
		if idx == n {
			err := d.DeleteCurrent()
			if err != nil {
				return err
			}
			return nil
		}
		d.Next()
	}
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	switch d.curr {
	case nil:
		return errors.New("current node is nil")
	case d.head:
		d.Shift()
		return nil
	case d.tail:
		d.Pop()
		return nil
	}
	d.curr.prev.next = d.curr.next
	d.curr = d.curr.next
	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	count := 0
	node := d.head
	for node != d.curr {
		node = node.next
		count++
	}
	return count, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.len <= 1 {
		d.head = nil
		d.tail = nil
		fmt.Println("THAT WAS THE LAST NODE")
		return nil
	}
	lastNode := d.tail
	d.tail = d.tail.prev
	if d.curr == lastNode {
		d.curr = d.tail
	}
	d.len--
	return lastNode
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.len <= 1 {
		d.head = nil
		d.tail = nil
		fmt.Println("THAT WAS THE LAST NODE")
		return nil
	}
	firstNode := d.head
	d.head = d.head.next
	if d.curr == firstNode {
		d.curr = d.head
	}
	d.len--
	return firstNode
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	node := d.head
	for {
		if node.data.UUID == uuID {
			return node
		}
		node = node.next
	}
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	node := d.head
	for {
		if node.data.Message == message {
			return node
		}
		node = node.next
	}
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	currentNode := d.head
	var nextInList *Node
	d.head, d.tail = d.tail, d.head
	for currentNode != nil {
		nextInList = currentNode.next
		currentNode.next, currentNode.prev = currentNode.prev, currentNode.next
		currentNode = nextInList
	}
	d.curr = d.head
	return d
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON(n int) {
	type generateJSON struct {
		Message string    `fake:"{sentence:5}" json:"message"`
		Uuid    string    `json:"uuid"`
		Date    time.Time `json:"date"`
	}

	dateJSON := make([]generateJSON, n)
	for i, product := range dateJSON {
		_ = gofakeit.Struct(&product)
		product.Uuid = gofakeit.UUID()
		dateJSON[i] = product
	}
	jsonByte, _ := json.Marshal(dateJSON)
	_ = os.WriteFile("/home/shroomov/go/src/gitlab.com/mikecircle/kata/module3/task/testGen.json", jsonByte, 0666)

}

func (d *DoubleLinkedList) PrintList() {
	node := d.head
	for i := 0; i < d.len; i++ {
		fmt.Println(i, node.data)
		node = node.next
	}
}

func main() {
	var lGen LinkedLister = &DoubleLinkedList{}
	GenerateJSON(3)
	err := lGen.LoadData("/home/shroomov/go/src/gitlab.com/mikecircle/kata/module3/task/testGen.json")
	if err != nil {
		panic(err)
	}
	lGen.PrintList()
	lGen.Next()
	lGen.Next()
	err = lGen.DeleteCurrent()
	if err != nil {
		panic(err)
	}

}
