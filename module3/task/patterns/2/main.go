package main

import (
	"fmt"
)

// AirConditioner is the interface that defines the methods for controlling the air conditioner
type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct{}

func (c *RealAirConditioner) TurnOn() {
	fmt.Println("Turning on")
}

func (c *RealAirConditioner) TurnOff() {
	fmt.Println("Turning off")
}

func (c *RealAirConditioner) SetTemperature(temp int) {
	fmt.Println("Temp is set to ", temp)
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (c *AirConditionerAdapter) TurnOn() {
	c.airConditioner.TurnOn()
}

func (c *AirConditionerAdapter) TurnOff() {
	c.airConditioner.TurnOff()
}

func (c *AirConditionerAdapter) SetTemperature(temp int) {
	c.airConditioner.SetTemperature(temp)
}

type AirConditionerProxy struct {
	adapter *AirConditionerAdapter
	auth    bool
}

func (c *AirConditionerProxy) TurnOn() {
	if !c.auth {
		fmt.Println("Access denied")
		return
	}
	c.adapter.TurnOn()
}

func (c *AirConditionerProxy) TurnOff() {
	if !c.auth {
		fmt.Println("Access denied")
		return
	}
	c.adapter.TurnOff()
}

func (c *AirConditionerProxy) SetTemperature(temp int) {
	if !c.auth {
		fmt.Println("Access denied")
		return
	}
	c.adapter.SetTemperature(temp)
}

func NewAirConditionerProxy(auth bool) *AirConditionerProxy {
	var cond RealAirConditioner
	condAdapter := AirConditionerAdapter{airConditioner: &cond}
	return &AirConditionerProxy{adapter: &condAdapter, auth: auth}
}

func main() {
	airConditioner := NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
