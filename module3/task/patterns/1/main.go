package main

import (
	"fmt"
	"reflect"
)

type Order struct {
	Product         string
	ProductPrice    float64
	ProductQuantity int
	PricingStrategy
}

type PricingStrategy interface {
	Calculate(order Order) float64
}

type RegularPricing struct {
}

type SalePricing struct {
	DiscountAmount float64
}

func (r RegularPricing) Calculate(order Order) float64 {
	return order.ProductPrice * float64(order.ProductQuantity)
}

func (r SalePricing) Calculate(order Order) float64 {
	total := order.ProductPrice * float64(order.ProductQuantity)
	discount := total / 100 * r.DiscountAmount
	return total - discount
}

func (o *Order) SetPricingStrategy(strategy PricingStrategy) {
	o.PricingStrategy = strategy
}

func (o *Order) CheckOut() float64 {
	return o.PricingStrategy.Calculate(*o)
}

func newOrder(name string, price float64, quantity int) *Order {
	return &Order{Product: name, ProductPrice: price, ProductQuantity: quantity}
}

func main() {
	order := newOrder("Sneakers", 199.99, 2)
	pricingSlice := []PricingStrategy{RegularPricing{}, SalePricing{DiscountAmount: 5}, SalePricing{DiscountAmount: 10}, SalePricing{DiscountAmount: 15}}
	for i, v := range pricingSlice {
		order.SetPricingStrategy(v)
		fmt.Printf("%v: Total cost with %v strat: %v\n", i, reflect.TypeOf(order.PricingStrategy), order.CheckOut())
	}
}
