package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature() int
	GetHumidity() int
	GetWindSpeed() int
	GetWeatherResponse(location string) error
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey   string
	location string
	response WeatherResponse
}

type WeatherResponse struct {
	Location struct {
		Name string `json:"name"`
	} `json:"location"`
	Current struct {
		TempC    float64 `json:"temp_c"`
		WindKph  float64 `json:"wind_kph"`
		Humidity int     `json:"humidity"`
	} `json:"current"`
}

func (o *OpenWeatherAPI) GetWeatherResponse(location string) error {
	o.location = location
	request := fmt.Sprintf("https://api.weatherapi.com/v1/current.json?key=%v&q=%v&aqi=yes", o.apiKey, location)
	response, err := http.Get(request)
	if err != nil {
		return err
	}
	log.Printf("request: %v, status: %v\n", request, response.Status)

	responseData, err := io.ReadAll(response.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(responseData, &o.response)
	if err != nil {
		return err
	}
	return nil
}

func (o *OpenWeatherAPI) GetTemperature() int {
	return int(o.response.Current.TempC)
}

func (o *OpenWeatherAPI) GetHumidity() int {
	return o.response.Current.Humidity
}

func (o *OpenWeatherAPI) GetWindSpeed() int {
	return int(o.response.Current.WindKph)
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {
	err := w.weatherAPI.GetWeatherResponse(location)
	if err != nil {
		panic(err)
	}
	temperature := w.weatherAPI.GetTemperature()
	humidity := w.weatherAPI.GetHumidity()
	windSpeed := w.weatherAPI.GetWindSpeed()

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("f2323cfa029a42ad9ea201646233005")
	//cities := []string{"Москва", "Санкт-Петербуг", "Казань", "Якутск"}
	cities := []string{"Moscow", "Saint-Petersburg", "Kazan", "Yakutsk"}
	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d\n\n", windSpeed)
	}
}
