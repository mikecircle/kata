package main

import (
	"errors"
	"fmt"
	"time"
)

type Post struct {
	body        string
	publishDate int64 // Unix timestamp
	next        *Post
}

type Feed struct {
	length int // we'll use it later
	start  *Post
	end    *Post
}

// Реализуйте метод Append для добавления нового поста в конец потока

func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		lastPost := f.end
		lastPost.next = newPost
		f.end = newPost
	}
	fmt.Printf("Adding a post (%v) at %v\n", newPost.body, time.Unix(newPost.publishDate, 0))
	f.length++
}

// Реализуйте метод Remove для удаления поста по дате публикации из потока

func (f *Feed) Remove(publishDate int64) {
	if f.length == 0 {
		panic(errors.New("feed is empty"))
	}
	fmt.Println("Deleting a post from", time.Unix(publishDate, 0))
	var previousPost *Post
	currentPost := f.start
	for currentPost.publishDate != publishDate {
		previousPost = currentPost
		currentPost = currentPost.next
	}
	previousPost.next = currentPost.next
	f.length--
}

// Реализуйте метод Insert для вставки нового поста в поток в соответствии с его датой публикации

func (f *Feed) Insert(newPost *Post) {
	fmt.Printf("Inserting `%v` at %v\n", newPost.body, time.Unix(newPost.publishDate, 0))
	if f.length == 0 {
		f.start = newPost
	} else {
		var previousPost *Post
		currentPost := f.start

		for currentPost.publishDate < newPost.publishDate {
			previousPost = currentPost
			currentPost = previousPost.next
		}

		previousPost.next = newPost
		newPost.next = currentPost
	}
	f.length++
}

// Реализуйте метод Inspect для вывода информации о потоке и его постах

func (f *Feed) Inspect() {
	if f.length == 0 {
		fmt.Println("Feed is empty")
	}
	var previousPost *Post
	currentPost := f.start
	fmt.Println("================")
	fmt.Println("Feed length is", f.length)
	for i := 0; i < f.length; i++ {
		fmt.Printf("item %v: date: %v, body %v\n", i, currentPost.body, time.Unix(currentPost.publishDate, 0))
		previousPost = currentPost
		currentPost = currentPost.next
	}
	_ = previousPost
	fmt.Println("================")
}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow + 10,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 20,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 30,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 40,
	}
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)
	f.Append(p4)
	f.Inspect()
	f.Remove(rightNow + 20)
	f.Inspect()

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 11,
	}

	f.Insert(newPost)
	f.Inspect()
}
