package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
}

//func test(r interface{}) {
//	fmt.Printf("%T, %v\n", r, r)
//	switch r.(type) {
//	case *int:
//		{
//			if r.(*int) == nil {
//				fmt.Println("Success!")
//			}
//		}
//	}
//}
