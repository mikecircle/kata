package main

import "fmt"

type User struct {
	Age    int
	Name   string
	Wallet Wallet
	//Location Location
}

//type Location struct {
//	Address string
//	City    string
//	Index   string
//}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	testArray()
	rangeArray()

}

func testArray() {
	a := [...]int{34, 55, 89, 144}
	fmt.Println("orig value", a)
	a[0] = 21
	fmt.Println("izmenenniy perviy el:", a)
	b := a
	a[0] = 233
	fmt.Println("orig arr", a)
	fmt.Println("modified arr", b)
}

func rangeArray() {
	user := [...]User{
		{
			Age:  8,
			Name: "joh",
			Wallet: Wallet{
				RUR: 13,
				USD: 1,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			Age:  13,
			Name: "katya",
			Wallet: Wallet{
				RUR: 500,
				USD: 3,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			Age:  21,
			Name: "Do",
			Wallet: Wallet{
				RUR: 0,
				USD: 300,
				BTC: 1,
				ETH: 3,
			},
		},
		{
			Age:  34,
			Name: "Arnue",
			Wallet: Wallet{
				RUR: 123123,
				USD: 34,
				BTC: 1,
				ETH: 2,
			},
		},
	}
	for i := range user {
		if user[i].Age > 18 {
			fmt.Println(user[i])
		}
	}
}
