package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Eva",
			Age:  13,
		},
		{
			Name: "Victor",
			Age:  28,
		},
		{
			Name: "Dex",
			Age:  34,
		},
		{
			Name: "Billy",
			Age:  21,
		},
		{
			Name: "Foster",
			Age:  29,
		},
	}

	subUsers := users[2:]
	editSecondSlice(subUsers)

	fmt.Println(users)
}

func editSecondSlice(users []User) {
	var users2 []User
	copy(users2, users)
	for i := range users2 {
		users[i].Name = "unknown"
	}
}
