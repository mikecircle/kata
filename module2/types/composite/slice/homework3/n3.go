package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func shiftpop(s *[]User) {
	newSlice := *s
	newSlice = newSlice[1:]
	newSlice = newSlice[:len(newSlice)-1]
	*s = newSlice
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	shiftpop(&users)
	fmt.Println(users)
}
