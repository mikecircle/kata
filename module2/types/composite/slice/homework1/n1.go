package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	k := []int{1, 2, 3}
	fmt.Printf("%v, %d\n", s, cap(s))
	Append(&s)
	fmt.Printf("%v, %d\n", s, cap(s))
	k = Append2(k)
	fmt.Printf("%v, %d\n", k, cap(s))

}

func Append(s *[]int) {
	*s = append(*s, 4)
}

func Append2(s []int) []int {
	return append(s, 4)
}
