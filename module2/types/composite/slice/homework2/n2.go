package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func find40(s []User) []User {
	var o []User
	for _, i := range s {
		if i.Age < 40 {
			o = append(o, i)
		}
	}
	return o
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	users = find40(users)
	fmt.Println(users[1])
}
