package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Location struct {
	Address string
	City    string
	Index   string
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	user := User{
		Age:  100,
		Name: "Dimas",
	}
	user2 := User{
		Age:  25,
		Name: "Egor",
		Wallet: Wallet{
			RUR: 1000000,
			USD: 123123,
			BTC: 1,
			ETH: 1,
		},
		Location: Location{
			Address: "yo, 11, 09",
			City:    "Elektrostal",
			Index:   "144010",
		},
	}
	fmt.Println(user)
	fmt.Println(user2)
	wallet := Wallet{
		RUR: 25000,
		USD: 2131,
		BTC: 1,
		ETH: 4,
	}
	fmt.Println(wallet)
	fmt.Println("wallet is", unsafe.Sizeof(wallet), "bytes")
	user.Wallet = wallet
	fmt.Println(user)
}
