package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeInt()
}

func typeInt() {
	var uintNumber uint16 = 1 << 15
	var from = int16(uintNumber)
	uintNumber--
	var to = int16(uintNumber)
	fmt.Println(from, to)
	numberInt8 := (1 << 7) - 1
	fmt.Println("uint8 max value:", numberInt8, "size:", unsafe.Sizeof(numberInt8), "byte")
	var numberInt16 int16 = (1 << 15) - 1
	fmt.Println("uint16 max value:", numberInt16, "size:", unsafe.Sizeof(numberInt16), "bytes")
	var numberInt32 int32 = (1 << 31) - 1
	fmt.Println("uint32 max value:", numberInt32, "size:", unsafe.Sizeof(numberInt32), "bytes")
	var numberInt64 int64 = (1 << 63) - 1
	fmt.Println("uint64 max value:", numberInt64, "size:", unsafe.Sizeof(numberInt64), "bytes")
}
