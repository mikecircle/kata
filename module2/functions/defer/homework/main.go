package main

import (
	"errors"
	"fmt"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

var (
	dictsOk        = MergeDictsJob{Dicts: []map[string]string{{"a": "b", "GO": "LANG", "1": "2"}, {"b": "c"}}}
	dictsNil       = MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, {"a": "b"}, {"a": "b"}, nil, {"a": "b"}, {"a": "b"}}}
	dictsNotEnough = MergeDictsJob{Dicts: []map[string]string{{"a": "b"}}}
)

// BEGIN (write your solution here)
func main() {
	var dicts = []MergeDictsJob{dictsOk, dictsNil, dictsNotEnough}
	for _, v := range dicts {
		_, err := ExecuteMergeDictsJob(&v)
		if err != nil {
			fmt.Printf("err: %v\n", err)
		}
		fmt.Printf("Dicts: %v, Merged: %v, Finished: %v\n\n", v.Dicts, v.Merged, v.IsFinished)
	}
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (map[string]string, error) {
	defer func() { job.IsFinished = true }()
	if len(job.Dicts) < 2 {
		return nil, errNotEnoughDicts
	}
	job.Merged = make(map[string]string)
	for n, k := range job.Dicts {
		if k == nil {
			job.Merged = nil
			return job.Merged, errNilDict //return *job, errNilDict
		}
		for i, v := range job.Dicts[n] {
			job.Merged[i] = v
		}
	}
	job.IsFinished = true
	return job.Merged, nil
}

// END

// Пример работы
// ExecuteMergeDictsJob(&MergeDictsJob{}) // &MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},nil}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},{"b": "c"}}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil
