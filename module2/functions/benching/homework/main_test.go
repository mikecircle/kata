package main

import "testing"

func BenchmarkMapUserProducts(b *testing.B) {
	var (
		users    = GenUsers()
		products = GenProducts()
	)
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkMapUserProducts2(b *testing.B) {
	var (
		users2    = GenUsers()
		products2 = GenProducts()
	)
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users2, products2)
	}
}
