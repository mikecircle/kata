package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var tick = time.NewTicker(1 * time.Second) //var tick = time.NewTicker(30 * time.Second)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			case <-tick.C:
				close(out)
				return
			}
		}
	}()

	return out
}

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	go func() {
		for num := range out {
			a <- num
		}
		close(a)
	}()

	go func() {
		for num := range out {
			b <- num
		}
		close(b)
	}()

	go func() {
		for num := range out {
			c <- num
		}
		close(c)
	}()

	mainChan := joinChannels(a, b, c)

	for num := range mainChan {
		fmt.Println(num)
	}
	_, okA := <-a
	_, okB := <-b
	_, okC := <-c
	_, okMain := <-mainChan
	fmt.Println(okMain, okC, okB, okA)
}
