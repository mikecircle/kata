package main

import (
	"fmt"
	"unicode"
)

func Greet(name string) string {
	if unicode.Is(unicode.Latin, []rune(name)[0]) {
		return fmt.Sprintf("Hello %s, you welcome!", name)
	} else {
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	}
}

//func main() {
//	Greet("Боря")
//}
