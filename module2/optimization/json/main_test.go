package main

import (
	"os"
	"testing"
)

func BenchmarkUnmarshalPets_builtin(b *testing.B) {
	var (
		pets Pets
		data []byte
	)
	jsonData, err := os.ReadFile("module2/optimization/json/jays0n.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets(jsonData)
		_ = err
		data, err = pets.Marshal()
		_ = err
	}
	_ = data
}

func BenchmarkUnmarshalPets_jsoniter(b *testing.B) {
	var (
		pets Pets
		data []byte
	)
	jsonData, err := os.ReadFile("module2/optimization/json/jays0n.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPetsJsoniter(jsonData)
		_ = err
		data, err = pets.MarshalJsoniter()
		_ = err
	}
	_ = data
}
