package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	jsoniter "github.com/json-iterator/go"
)

type Pets []Pet

func UnmarshalPetsJsoniter(data []byte) (Pets, error) {
	var r Pets
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) MarshalJsoniter() ([]byte, error) {
	return jsoniter.Marshal(r)
}

func UnmarshalPets(data []byte) (Pets, error) {
	var r Pets
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Pet struct {
	ID        float64    `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

func main() {
	file, err := os.ReadFile("module2/optimization/json/jays0n.json")
	if err != nil {
		log.Print(err)
	}
	petsBuiltin, err := UnmarshalPets(file)
	if err != nil {
		log.Print(err)
	}
	fmt.Println(petsBuiltin)
}
