package main

import (
	"testing"
)

func BenchmarkFilterText_SanitizeText(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeText(data[i])
		}

	}
}

func BenchmarkFilterText_SanitizeText1(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeText1(data[i])
		}

	}
}
