package main

import "testing"

type tests struct {
	name string
	a    float64
	b    float64
	want float64
}

func TestDivide(t *testing.T) {
	test := []tests{
		{
			name: "divide 2 by 2",
			a:    2,
			b:    2,
			want: 1,
		},
		{
			name: "divide 4 by 2",
			a:    4,
			b:    2,
			want: 2,
		},
		{
			name: "divide 8 by 2",
			a:    8,
			b:    2,
			want: 4,
		},
		{
			name: "divide 3 by 2",
			a:    3,
			b:    2,
			want: 1.5,
		},
		{
			name: "divide 1921.2141 by 12.890",
			a:    1921.2141,
			b:    12.890,
			want: 149.0468657874321,
		},
		{
			name: "divide 1921.2141 on 1",
			a:    1921.2141,
			b:    1,
			want: 1921.2141,
		},
	}
	calc := NewCalc()
	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			if got := calc.SetA(tt.a).SetB(tt.b).Do(divide).Result(); got != tt.want {
				t.Errorf("divide = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSum(t *testing.T) {
	test := []tests{
		{
			name: "sum 2 and 2",
			a:    2,
			b:    2,
			want: 4,
		},
		{
			name: "sum 4 and 2",
			a:    4,
			b:    2,
			want: 6,
		},
		{
			name: "sum 8 and 2",
			a:    8,
			b:    2,
			want: 10,
		},
		{
			name: "sum 3 and 2",
			a:    3,
			b:    2,
			want: 5,
		},
		{
			name: "sum 1921.2141 and 12.890",
			a:    1921.2141,
			b:    12.890,
			want: 1934.1041,
		},
		{
			name: "sum 1921.2141 and -12.890",
			a:    1921.2141,
			b:    -12.890,
			want: 1908.3240999999998,
		},
	}
	calc := NewCalc()
	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			if got := calc.SetA(tt.a).SetB(tt.b).Do(sum).Result(); got != tt.want {
				t.Errorf("sum = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMultiply(t *testing.T) {
	test := []tests{
		{
			name: "multiply 2 and 2",
			a:    2,
			b:    2,
			want: 4,
		},
		{
			name: "multiply 4 and 2",
			a:    4,
			b:    2,
			want: 8,
		},
		{
			name: "multiply 8 and 2",
			a:    8,
			b:    2,
			want: 16,
		},
		{
			name: "multiply 3 and 2",
			a:    3,
			b:    2,
			want: 6,
		},
		{
			name: "multiply 1921.2141 and 12.890",
			a:    1921.2141,
			b:    12.890,
			want: 24764.449749,
		},
		{
			name: "multiply -1921.2141 and 12.890",
			a:    -1921.2141,
			b:    12.890,
			want: -24764.449749,
		},
	}
	calc := NewCalc()
	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			if got := calc.SetA(tt.a).SetB(tt.b).Do(multiply).Result(); got != tt.want {
				t.Errorf("divide = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAvg(t *testing.T) {
	test := []tests{
		{
			name: "avg 2 and 2",
			a:    2,
			b:    2,
			want: 2,
		},
		{
			name: "avg 4 and 2",
			a:    4,
			b:    2,
			want: 3,
		},
		{
			name: "multiply 8 and 2",
			a:    8,
			b:    2,
			want: 5,
		},
		{
			name: "avg 3 and 2",
			a:    3,
			b:    2,
			want: 2.5,
		},
		{
			name: "avg 1921.2141 and 12.890",
			a:    1921.2141,
			b:    12.890,
			want: 967.05205,
		},
	}
	calc := NewCalc()
	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			if got := calc.SetA(tt.a).SetB(tt.b).Do(average).Result(); got != tt.want {
				t.Errorf("divide = %v, want %v", got, tt.want)
			}
		})
	}
}
