package main

import (
	"encoding/json"
	"fmt"
)

// Student declares `Student` structure
type Student struct {
	FirstName, LastName string
	Email               string `json:"email "`
	Age                 int
	HeightInMeters      float64
	IsMale              bool
}

func main() {

	// define `john` struct
	john := Student{
		FirstName:      "John",
		LastName:       "Doe",
		Age:            21,
		HeightInMeters: 1.75,
		IsMale:         true,
	}

	// encode `john` as JSON
	johnJSON, _ := json.Marshal(john)

	// print JSON string
	fmt.Println(string(johnJSON))
	fmt.Println(json.Valid(johnJSON))
}
