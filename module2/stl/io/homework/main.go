package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}

	// здесь расположите буфер
	var (
		buffer    bytes.Buffer
		newBuffer bytes.Buffer
	)
	// запишите данные в буфер
	for _, i := range data {
		buffer.WriteString(i + "\n")
	}
	// создайте файл
	file, err := os.Create("/home/shroomov/Templates/newfile.txt")
	if err != nil {
		panic(err)
	}
	// запишите данные в файл
	_, err = io.Copy(file, &buffer)
	if err != nil {
		panic(err)
	}
	// прочтите данные в новый буфер
	fileContent, _ := os.ReadFile("/home/shroomov/Templates/newfile.txt")
	newBuffer.WriteString(string(fileContent))
	fmt.Println(data) // выведите данные из буфера buffer.String()
	fmt.Println(newBuffer.String())
}
