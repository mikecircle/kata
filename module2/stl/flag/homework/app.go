package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"an"`
	Production bool   `json:"prod"`
}

func main() {
	var fileName string
	flag.StringVar(&fileName, "conf", "", "specify file location")
	flag.Parse()
	config := &Config{}
	if fileName != "" {
		fileContent, err := os.ReadFile(fileName)
		if err != nil {
			panic(err)
		}
		err = json.Unmarshal(fileContent, &config)
		if err != nil {
			panic(err)
		}
	}
	fmt.Printf("Production: %v\nAppName: %v\n", config.Production, config.AppName)
}
