package main

import (
	"bufio"
	"fmt"
	"os"

	transliting "gitlab.com/mikecircle/kata/module2/stl/utf8/homework"
)

func main() {
	exerciseOne()
	exerciseTwo()
}

func exerciseOne() {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)
	err := os.WriteFile("/home/shroomov/Templates/one.txt", []byte(name), 0644)
	if err != nil {
		panic(err)
	}
}

func exerciseTwo() {
	theString := "Съешь же ещё этих мягких французских булок, да выпей чаю!"
	f, _ := os.Create("/home/shroomov/Templates/example.txt")
	f2, _ := os.Create("/home/shroomov/Templates/example.processed.txt")

	_, err := f.WriteString(theString)
	if err != nil {
		panic(err)
	}

	of, _ := os.ReadFile("/home/shroomov/Templates/example.txt")
	ofTranslit := transliting.ToTranslit(string(of))

	_, err = f2.WriteString(ofTranslit)
	if err != nil {
		panic(err)
	}
}
