package transliting

import (
	"strings"
)

//var data = ` Сообщения с телеграм канала Go-go!
//я, извините, решил, что вы хоть что-то про регулярки знаете 🙂
//С Почтением, с уважением 🙂.
//
//Сообщения с телеграм канала Go-get job:
//#Вакансия #Vacancy #Удаленно #Remote #Golang #Backend
//👨‍💻  Должность: Golang Tech Lead
//🏢  Компания: PointPay
//💵  Вилка: от 9000 до 15000 $ до вычета налогов
//🌏  Локация: Великобритания
//💼  Формат работы: удаленный
//💼  Занятость: полная занятость
//
//Наша компания развивает 15 продуктов в сфере блокчейн и криптовалюты, функционирующих в рамках единой экосистемы. Мы - одни из лучших в своей нише, и за без малого три года мы создали криптовалютную платформу, которая предоставляет комплексные решения и позволяет пользователям проводить практически любые финансовые операции.
//Наш проект победил в номинации "лучший блокчейн-стартап 2019 года" на одной из самых крупных конференций по блокчейн, криптовалютам и майнингу – Blockchain Life в Москве.
//
//У нас очень амбициозные планы по развитию наших продуктов, и на данный момент мы находимся в поиске Golang Technical Lead, чтобы вместе создать революционный и самый технологичный продукт в мире.
//
//
//
//Мы ожидаем от тебя:
//
//✅ Опыт управления финансовыми IT продуктами;
//✅ Опыт постановки задач технической команде и приема результатов;
//✅ Понимание и использование современных подходов к разработке (Agile);
//✅ Опыт разработки не менее 5 лет;
//✅ Отличное владение Go, будет плюсом опыт работы на PHP;
//✅ Опыт работы с REST / GRPС (protobuf);
//✅ Опыт работы с Laravel;
//✅ Отличные навыки SQL (мы используем PostgreSQL);
//✅ Опыт работы с Redis или аналогичной системой кеширования;
//✅ Опыт работы с Kubernetes, с брокерами сообщений (RabbitMQ, Kafka и др.);
//✅ Базовые знания Amazon Web Services.
//
//Будем плюсом:
//
//✅ Опыт разработки криптовалютных кошельков / криптовалютных бирж / криптовалютных торговых роботов / криптовалютных платежных систем или опыт работы в FinTech или банковских продуктах.
//
//Чем предстоит заниматься:
//
//📌 Управлением и взаимодействием с командой;
//📌 Постановкой задач команде разработчиков;
//📌 Разработкой решений для запуска и сопровождения разрабатываемого компанией ПО;
//📌 Выстраиванием эффективного процесса разработки;
//📌 Достигать результатов, поддерживать и развивать текущие проекты, развивать новые проекты и продукты, искать и предлагать нестандартные решения задач.
//
//Мы гарантируем будущему коллеге:
//
//🔥 Высокую заработную плату до 15 000$ (в валюте);
//🔥 Индексацию заработной платы;
//🔥 Полностью удаленный формат работы и максимально гибкий график - работайте из любой точки мира в удобное вам время;
//🔥 Оформление по бессрочному международному трудовому договору с UK;
//🔥 Отсутствие бюрократии и кучи бессмысленных звонков;
//🔥 Корпоративную технику за счет компании;
//🔥 Сотрудничество с командой высококвалифицированных специалистов, настоящих профессионалов своего дела;
//🔥 Работу над масштабным, интересным и перспективным проектом.
//
//
//Если в этом описании вы узнали  себя - пишите. Обсудим проект, задачи и все детали :)
//
//Павел,
//✍️TG: @pavel_hr
//📫 papolushkin.wanted@gmail.com`

//func main() {
//	fmt.Println("runes: ", utf8.RuneCountInString(data))
//	data = replaceEmojis(data)
//	fmt.Println("runes without emojis:", utf8.RuneCountInString(data))
//	dataLatin := ToTranslit(data)
//	fmt.Println("runes in translit:", utf8.RuneCountInString(dataLatin))
//	LatinByteCount, CyrillicByteCount := float64(len(dataLatin)), float64(len(data))
//	res := LatinByteCount / CyrillicByteCount
//	fmt.Println("compression is: ", res)
//}

func MakeUppercaseTranslitMap() map[string]string {
	translitMapUpper := make(map[string]string, 33)
	for j := range TranslitMap {
		if len(TranslitMap[j]) > 1 {
			firstLetter, remainingLetters := strings.ToUpper(strings.Split(TranslitMap[j], "")[0]), strings.Split(TranslitMap[j], "")[1:]
			combinedLetters := firstLetter + strings.Join(remainingLetters, "")
			translitMapUpper[strings.ToUpper(j)] = combinedLetters
		} else {
			translitMapUpper[strings.ToUpper(j)] = strings.ToUpper(TranslitMap[j])
		}
	}
	return translitMapUpper
}

func ToTranslit(s string) string {
	ss := strings.Split(s, "")
	translitMapUpper := MakeUppercaseTranslitMap()
	for _, v := range ss {
		_, okLower := TranslitMap[v]
		_, okUpper := translitMapUpper[v]
		switch {
		case okUpper:
			{
				s = strings.ReplaceAll(s, v, translitMapUpper[v])
			}
		case okLower:
			{
				s = strings.ReplaceAll(s, v, TranslitMap[v])
			}
		}
	}
	return s
}

//func replaceEmojis(s string) string {
//	for ss := s; len(ss) > 0; {
//		j, size := utf8.DecodeRuneInString(ss)
//		switch {
//		case j == 65039 || j == 8205 || j == 128187: //зато без сторонних библиотек
//			{
//				s = strings.ReplaceAll(s, string(j), "")
//			}
//		case size > 2 && string(j) != "–":
//			{
//				s = strings.ReplaceAll(s, string(j), "=)")
//			}
//		}
//		ss = ss[size:]
//	}
//	return s
//}

var TranslitMap = map[string]string{
	"а": "a",
	"б": "b",
	"в": "v",
	"г": "g",
	"д": "d",
	"е": "e",
	"ё": "yo",
	"ж": "zh",
	"з": "z",
	"и": "i",
	"й": "y",
	"к": "k",
	"л": "l",
	"м": "m",
	"н": "n",
	"о": "o",
	"п": "p",
	"р": "r",
	"с": "s",
	"т": "t",
	"у": "u",
	"ф": "f",
	"х": "kh",
	"ц": "ts",
	"ч": "ch",
	"ш": "sh",
	"щ": "sch",
	"ъ": "",
	"ы": "y",
	"ь": "",
	"э": "e",
	"ю": "yu",
	"я": "ya",
}
