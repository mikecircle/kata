package main

import (
	"fmt"
)

type Person struct {
	Name string
	Age  int
}

func generateSelfStory(person Person, f float64) string {
	return fmt.Sprintf("Privet, ya %v, mne %v let, a esche u menya est $%.2f deneg!!!", person.Name, person.Age, f)
}

func main() {
	p := Person{Name: "Andy", Age: 18}
	fmt.Println(generateSelfStory(p, 12.231322))
}
