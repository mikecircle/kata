package main

import "fmt"

func main() {
	arguments := make(chan int)
	done := make(chan struct{})
	result := calculator(arguments, done)
	for i := 0; i < 10; i++ {
		arguments <- 1
	}
	close(done)
	fmt.Println(<-result)
}

func calculator(arguments <-chan int, done <-chan struct{}) <-chan int {
	out := make(chan int)
	result := 0
	go func() {
		for {
			select {
			case v := <-arguments:
				result += v
			case <-done:
				out <- result
				close(out)
				return
			}
		}
	}()
	return out
}
