package main

import (
	"math/rand"
	"strings"
	"testing"
	"time"
)

var input = GenNumbers()

func GenNumbers() []string {
	rand.Seed(time.Now().UnixNano())
	s := make([]string, 4200000)
	for i := range s {
		c := rand.Intn(10)
		ones, zeros := strings.Repeat("1", c), strings.Repeat("0", 10-c)
		s[i] = ones + zeros
	}
	return s
}

func BenchmarkBattery_Convert2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Battery.Convert2(Battery(input[i]))
	}
}

func BenchmarkBattery_Convert1(b *testing.B) {
	//var input = GenNumbers()
	for i := 0; i < b.N; i++ {
		Battery.Convert1(Battery(input[i]))
	}
}

func BenchmarkBattery_String(b *testing.B) {
	//input := GenNumbers()
	for i := 0; i < b.N; i++ {
		Battery.String(Battery(input[i]))
	}
}
