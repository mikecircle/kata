package main

import (
	"fmt"
	"strings"
)

type Battery string

func (c Battery) Convert2() string {
	//return fmt.Sprintf("[%10s]", strings.Repeat("X", strings.Count(string(c), `1`)))
	xs := ""
	for _, i := range c {
		if string(i) == "1" {
			xs += "X"
		}
	}
	zeros := "          "
	zeros = zeros[len(xs):]
	return "[" + zeros + xs + "]"
}
func (c Battery) Convert1() string {
	xs := ""
	zeros := ""
	for _, i := range c {
		if string(i) == "0" {
			zeros += " "
		} else {
			xs += "X"
		}
	}
	return "[" + zeros + xs + "]"
}

func (c Battery) String() string {
	xs := strings.Count(string(c), "1")
	zeros := strings.Repeat(" ", 10-xs)
	return "[" + zeros + strings.Repeat("X", xs) + "]"
}

func main() {

	var batteryForTest Battery = "1010101010"
	//fmt.Scan(&batteryForTest)
	//batteryForTest =
	fmt.Println(batteryForTest.Convert2())
	fmt.Println(batteryForTest.Convert1())
	fmt.Println(batteryForTest.String())
	//}
}
