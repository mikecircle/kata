module gitlab.com/mikecircle/kata

go 1.18

require (
	github.com/brianvoe/gofakeit/v6 v6.21.0
	gitlab.com/mikecircle/greet v0.0.0-20230415144306-c7c2400b87ae
	golang.org/x/sync v0.1.0
)

require (
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
)
